#!/usr/bin/env python

"""
Base Genetic Algorithm with Real Encoding


Roulette-based selection
Random mutation
Optional elitism
"""
from cec2019comp100digit import cec2019comp100digit
import random as rand
import numpy as np
import sys
import csv
from datetime import date

import os
from cec2019comp100digit import cec2019comp100digit


class RealGA:

    def __init__(self, init_pop=2, max_cycles=1, opc=1):
        rand.seed()

        self.best = []
        self.bench = cec2019comp100digit
        # self.max_pop_size = max_pop_size
        self.opc = opc
        self.pop_size = init_pop
        # set max and min values
        if opc == 1:
            self.min = -8192.
            self.max = 8192.
            self.dim = 9
        elif opc == 2:
            self.min = -16384.
            self.max = 16384.
            self.dim = 16
        elif opc == 3:
            self.min = -4.
            self.max = 4.
            self.dim = 18
        elif opc == 4:
            self.min = -100.
            self.max = 100.
            self.dim = 10
        elif opc >= 5 and opc <= 10:
            self.min = -100.
            self.max = 100.
            self.dim = 10
        else:
            print("no go")
            sys.exit()

        self.max_cycles = max_cycles

        self.count = 0  # benchmark function calls

        self.population = [
            {
                'eval': 0.,
                'x': [round(rand.uniform(self.min, self.max), 9) for i in range(self.dim)],
            } for x in range(init_pop)
        ]

        self.children = []

        self.population = self.eval_pop(self.population)

        aux = self.population.copy()

        aux.sort(key=self.getFitness)

        self.best.append(aux[0])

        # print("******** INITIAL POPULATION ********")
        # for ind in self.population:
        #     # print(ind)
        #     print(str(ind['eval']) +
        #           " " + "\n")

    def selection(self):
        self.selected = []

        # Tournament selection
        for i in range(len(self.population)):
            r = rand.randrange(0, len(self.population))
            if self.population[i]['eval'] <= self.population[r]['eval']:
                self.selected.append(self.population[i])
            else:
                self.selected.append(self.population[r])

    def crossover(self):

        for (i, ind) in enumerate(self.selected):
            first = {
                'eval': 0.,
                'x': [],
            }
            second = {
                'eval': 0.,
                'x': [],
            }

            cross_point = rand.randrange(self.dim)

            if i % 2 == 0:
                partner = i + 1
            else:
                partner = i - 1

            if i == len(self.selected) - 1:
                partner = 0

            first['x'] += (ind['x'][:cross_point])
            first['x'] += (self.selected[partner]['x']
                           [cross_point:])

            second['x'] += (self.selected[partner]['x'][:cross_point])
            second['x'] += (ind['x'][cross_point:])

            if len(self.children) < self.pop_size:
                self.children.append(first)
            if len(self.children) < self.pop_size:
                self.children.append(second)

    def mutation(self):
        rate = 0.1 / self.dim
        for child in self.children:
            rand_gene = rand.randrange(0, self.dim)

            r = rand.random()

            if r <= rate:
                child['x'][rand_gene] = round(
                    rand.uniform(self.min, self.max), 9)

        self.eval_pop(self.children)

    def elitism(self):
        # elitism
        aux1 = self.population.copy()
        aux1.sort(key=self.getFitness)

        el_len = int(self.pop_size * 0.1)

        elite = aux1[:el_len]

        aux = self.children.copy()
        aux.sort(key=self.getFitness)
        elite_c = aux[:self.pop_size-el_len]

        new_pop = elite + elite_c
        # print(new_pop)
        self.population = new_pop.copy()
        new_pop.sort(key=self.getFitness)
        self.best.append(new_pop[0])

    def getFitness(self, solution):
        return solution['eval']

    def eval_pop(self, population):
        for i in range(len(population)):
            self.bench.init(self.opc, self.dim)
            sol = np.array(population[i]['x'])
            ev = self.bench.eval(sol)
            self.bench.end()
            self.count += 1
            population[i]['eval'] = ev

        return population


if __name__ == '__main__':
    # problem selection
    pop_size = int(sys.argv[1])
    prob = int(sys.argv[2])

    # d = RealGA(pop_size, 1000000, prob)
    # g = 0

    # while(d.count < d.max_cycles):
    #     d.selection()
    #     d.crossover()
    #     d.mutation()
    #     d.elitism()
    #     d.children = []
    #     g += 1

    # print(str(str(d.best[-1]['eval'])))

    date = str(date.today())
    directory = "data" + "/" + date
    PARENT_FOLDER = os.path.dirname(os.path.abspath(__file__))
    path = os.path.join(PARENT_FOLDER, directory)

    try:
        os.makedirs(path)
        # print("Directory '%s' created successfully" % directory)
    except OSError as error:
        # print("Directory '%s' can not be created, already exists" % directory)
        pass

    for i in range(10):

        f_name = "report_stats_sga_tuning_" + date + \
            "_" + str(prob) + "_" + str(i) + ".txt"
        f_stat = os.path.join(path, f_name)

        stat_file = open(f_stat, "w")

        f_name = "report_best_sga_tuning_" + date + \
            "_" + str(prob) + "_" + str(i) + ".txt"
        f_sol = os.path.join(path, f_name)

        d = RealGA(1566, 1000000, prob)
        g = 0

        while(d.count < d.max_cycles):
            d.selection()
            d.crossover()
            d.mutation()
            d.elitism()
            d.children = []

            best = d.best[-1]['eval']

            fsum = 0
            for b in d.best:
                fsum += b['eval']
            avg = fsum / len(d.best)

            stat_file.write("{}\t{}\t{}\t{}\n".format(g, d.count, avg, best))

            g += 1

        print(str(str(d.best[-1]['eval'])))
        print('***** Best solutions after %d *****' % (g) + "\n")

        print(str(d.best[-1]['x']) + " " + str(d.best[-1]['eval']) + "\n")

        if stat_file:
            stat_file.close()

        if f_sol:
            solfile = open(f_sol, "w")
            for i in d.best[-1]['x']:
                solfile.write(str(i))
                solfile.write(" ")
            solfile.write("\n")
            solfile.close()

        # python rega.py 395 4

        # with open('data/res_ga_1e6_'+str(d.opc)+"_"+str(i)+'.csv', mode='w') as res_file:
        #     res_writer = csv.writer(res_file, delimiter=',',
        #                             quotechar='"', quoting=csv.QUOTE_MINIMAL)

        #     res_writer.writerow(['x1', 'eval'])
        #     for ind in d.best:
        #         res_writer.writerow([ind['x'], ind['eval']])
