#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# file  :  OneMax.py
# author:  Daniel H. Stolfi
# date  :  2019-08-15
#
# Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics.
# In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
# doi> 10.1016/j.ins.2017.10.005
# -*- coding: utf-8 -*-
"""
Example of the epiGenetic Algorithm solving the OneMax problem (Maximization, Binary Representation).
"""

from epiga.problems.binaryproblem import BinaryProblem
from epiga.epiga import EpiGA
from epiga.operators.nucleosomegenerator import NucleosomeGenerator
from epiga.operators.binarytournamentselection import BinaryTournamentSelection
from epiga.operators.nucleosomebasedreproduction import NucleosomeBasedReproduction
from epiga.operators.binarygenesilencing import BinaryGeneSilencing
from epiga.operators.elitistreplacement import ElitistReplacement


class OneMaxProblem(BinaryProblem):
    """OneMax Problem."""

    def __init__(self, size):
        """
        Constructor.

        :param int size: the number of values in the solution vector.
        """
        super().__init__("OneMax Problem", size, [0.75] * size, BinaryProblem.MAXIMIZATION)

    def _get_fitness(self, solution):
        return len([x for x in solution if x])


if __name__ == '__main__':
    L = 1000
    p = OneMaxProblem(L)
    alg = EpiGA(p, 20, 1, NucleosomeGenerator(3 / L, int(round(L / 8))), BinaryTournamentSelection(),
                NucleosomeBasedReproduction(), BinaryGeneSilencing(8 / L), ElitistReplacement(duplicates=False))
    alg.run(8000, 1)
