# -*- coding: utf-8 -*-
# file  :  nucleosomebasedreproduction.py
# author:  Daniel H. Stolfi
# date  :  2019-08-15
#
# Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics.
# In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
# doi> 10.1016/j.ins.2017.10.005
from ..epigeneticindividualmale import EpigeneticIndividualMale
from ..epigeneticindividualfemale import EpigeneticIndividualFemale
from ..epigeneticpopulationfemale import EpigeneticPopulationFemale
from ..epigeneticpopulationmale import EpigeneticPopulationMale
from .reproductionoperator import ReproductionOperator
from ..epigeneticcell import EpigeneticCell

import random as rand


class NucleosomeBasedReproduction(ReproductionOperator):
    """NucleosomeBasedReproduction Operator."""

    # todo
    # generar macho o hembra
    # ? dependendiendo de que?

    def _xor(self, x, y):
        return bool((x and not y) or (not x and y))

    def reproduction(self, popmale, popfem, problem, n_cells, gen):
        # todo
        # * tomar en cuenta inversión parental

        # se crean las dos poblaciones de
        # progenie
        daughters = EpigeneticPopulationFemale(problem)
        sons = EpigeneticPopulationMale(problem)

        for i in range(0, popmale.size):

            # obtaining ids
            mid = popfem.get(i).id
            pid = popmale.get(i).id

            # obtaining best cells from each parent
            c1 = popmale.get(i).get_best_cell()
            c2 = popfem.get(i).get_best_cell()

            # solution vectors
            x1 = c1.solution.copy()
            x2 = c2.solution.copy()

            # parents nucleosome vectors
            n1 = c1.nucleosomes.copy()
            n2 = c2.nucleosomes.copy()

            N = []  # new nucleosome
            nx1 = []  # new solution 1
            nx2 = []  # new solution 2

            for k in range(len(n1)):
                N.append(self._xor(n1[k], n2[k]))

            for k in range(len(N)):
                if N[k] == 1:
                    nx1.append(x1[k])
                    nx2.append(x2[k])
                else:
                    nx1.append(x2[k])
                    nx2.append(x1[k])

            cell1 = EpigeneticCell(problem, N, nx1, x1, x2)
            cell2 = EpigeneticCell(problem, N, nx2, x1, x2)

            cell1.evaluate()
            cell2.evaluate()

            new_ind = EpigeneticIndividualFemale(
                problem=problem,
                n_cells=n_cells,
                gen=gen+1,
                i=i,
                cells=None,
                pid=pid,
                mid=mid
            )

            new_ind.replace_worst(cell2)
            new_ind.evaluate()
            new_ind.eval_individual_fitness(popmale.get(i))  # provisional
            daughters.add(new_ind)

            new_ind = EpigeneticIndividualMale(
                problem=problem,
                n_cells=n_cells,
                gen=gen+1,
                i=i,
                cells=None,
                pid=pid,
                mid=mid
            )
            new_ind.evaluate()
            new_ind.replace_worst(cell1)
            sons.add(new_ind)

            # update mother energy
            if new_ind.is_better(popmale.get(i)):
                per = (popmale.get(i).get_best_fitness() /
                       sons.get(-1).get_best_fitness()) * 100
                popfem.get(i).energy -= per

            # toss = rand.random()

            # if toss <= 0.5:
            #     new_ind = EpigeneticIndividualFemale(problem, n_cells)
            #     if i == 0:
            #         new_ind.replace_worst(cell1)
            #     else:
            #         new_ind.replace_worst(cell2)
            #     daughters.add(new_ind)
            # else:
            #     new_ind = EpigeneticIndividualMale(problem, n_cells)
            #     if i == 0:
            #         new_ind.replace_worst(cell1)
            #     else:
            #         new_ind.replace_worst(cell2)
            #     sons.add(new_ind)

        return daughters, sons
