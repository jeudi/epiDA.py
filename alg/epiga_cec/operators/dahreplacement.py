# -*- coding: utf-8 -*-
# file  :  elitistreplacement.py
# author:  Daniel H. Stolfi
# date  :  2019-08-15
#
# Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics.
# In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
# doi> 10.1016/j.ins.2017.10.005
from .replacementoperator import ReplacementOperator
from ..epigeneticpopulation import EpigeneticPopulation
from ..epigeneticpopulationfemale import EpigeneticPopulationFemale
from ..epigeneticpopulationmale import EpigeneticPopulationMale
from ..epigeneticindividualfemale import EpigeneticIndividualFemale
from ..epigeneticindividualmale import EpigeneticIndividualMale

import random as rand


class DAHReplacement(ReplacementOperator):
    """DAH Replacement Operator."""

    def __init__(self, duplicates=True):
        """
        Constructor.

        :param bool duplicates: True (default) to allow repeated EpigeneticIndividuals in the resulting EpigeneticPopulation.
        """
        self.__duplicates = duplicates

    def replace(self, old_pop, children, sex):
        # we should:
        # * check if too old
        # * check if still energy left
        # * check survival probability
        alpha = 0.8  # survival probability

        if sex == "f":
            result = EpigeneticPopulationFemale(old_pop.problem)

            for i in range(old_pop.size):
                oldie = old_pop.get(i)
                child = children.get(i)
                age = oldie.age
                energy = oldie.energy

                if age < 10:
                    if energy > 0:
                        if rand.random() <= alpha:
                            # they compete
                            if oldie.is_better(child):
                                result.add(oldie)
                            else:
                                result.add(child)
                        else:  # dies of bad luck
                            result.add(child)
                    else:  # dies of weakness
                        result.add(child)
                else:  # dies of old age
                    result.add(child)

        elif sex == "m":
            # l1 = list()
            # for i in range(old_pop.size):
            #     l1.append(old_pop.get(i))
            # for i in range(children.size):
            #     l1.append(children.get(i))

            # l2 = sorted(l1, reverse=old_pop.problem.maximization,
            #             key=lambda x: x.get_best_fitness())

            # result = EpigeneticPopulationMale(old_pop.problem)
            # l = list()
            # i = 0
            # while i < old_pop.size and result.size < old_pop.size:
            #     v = hash(tuple(l2[i].get_best_cell().solution))
            #     if self.__duplicates or v not in l:
            #         result.add(l2[i].replicate())
            #         l.append(v)
            #     i += 1
            # for i in range(result.size, old_pop.size):
            #     result.add(EpigeneticIndividualMale(
            #         old_pop.problem, len(old_pop.get(0).cells)))
            result = EpigeneticPopulationMale(old_pop.problem)

            for i in range(old_pop.size):
                oldie = old_pop.get(i)
                child = children.get(i)
                age = oldie.age
                energy = oldie.energy

                if age < 10:
                    if energy > 0:
                        if rand.random() <= alpha:
                            # they compete
                            if oldie.is_better(child):
                                result.add(oldie)
                            else:
                                result.add(child)
                        else:  # dies of bad luck
                            result.add(child)
                    else:  # dies of weakness
                        result.add(child)
                else:  # dies of old age
                    result.add(child)

        return result
