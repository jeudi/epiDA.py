from epiga_cec.epigeneticindividualfemale import EpigeneticIndividualFemale
from epiga_cec.epigeneticindividualmale import EpigeneticIndividualMale
import random as rand

from .selectionoperator import SelectionOperator
from ..epigeneticpopulation import EpigeneticPopulation
from ..epigeneticpopulationfemale import EpigeneticPopulationFemale
from ..epigeneticpopulationmale import EpigeneticPopulationMale


class DAHSelection(SelectionOperator):
    """
    DAH Selection Operator

    This selection method pairs female individuals with male 
    individuals. Random males will compete for pairing with a given female.

    """

    def select(self, pop, sex: str, pop2=None, ):
        if sex == "f":
            temp = EpigeneticPopulationFemale(pop.problem)
        elif sex == "m":
            temp = EpigeneticPopulationMale(pop.problem)
        else:
            return 0

        if sex == "f":
            for i in range(pop.size):
                i1 = pop.get(rand.randint(0, pop.size - 1))
                i2 = pop.get(rand.randint(0, pop.size - 1))
                i1.eval_individual_fitness(pop2.get(i))
                i2.eval_individual_fitness(pop2.get(i))

                if i1.is_better(i2):
                    temp.add(i1)
                else:
                    temp.add(i2)
        elif sex == "m":
            for n in range(pop.size):
                i1 = pop.get(rand.randint(0, pop.size - 1))
                i2 = pop.get(rand.randint(0, pop.size - 1))
                if i1.is_better(i2):
                    temp.add(i1)
                else:
                    temp.add(i2)
        else:
            return 0
        return temp
