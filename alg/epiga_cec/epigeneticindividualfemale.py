# -*- coding: utf-8 -*-
# file  :  epigeneticindividual.py
# author:  Daniel H. Stolfi
# date  :  2019-08-15
#
# Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics.
# In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
# doi> 10.1016/j.ins.2017.10.005

from .epigeneticindividualmale import EpigeneticIndividualMale
from .epigeneticcell import EpigeneticCell
from .problems.problem import Problem


class EpigeneticIndividualFemale:
    """Epigenetic Individual."""

    def __init__(self, problem: Problem, n_cells: int,  gen=None, i=None, cells=None, pid=None, mid=None):
        """
        Constructor.

        :param Problem problem: the problem.
        :param int n_cells: the number of cells
        :param list[EpigeneticCell] cells: an optional list of EpigeneticCells of size n_cells.
        """
        self.__problem = problem
        if cells is None:
            self.__cells = [EpigeneticCell(self.__problem)
                            for _ in range(n_cells)]
        else:
            self.__cells = cells

        self.__age = 1
        self.__energy = 100
        self.__quality = 0
        self.__id = ""
        self.__pid = ""
        self.__mid = ""
        if gen is not None and i is not None:
            self.__id = str(gen) + "_" + str(i)
        if pid is not None:
            self.__pid = pid
        if mid is not None:
            self.__mid = mid

    def __str__(self):
        string = ""
        string += "Quality: " + str(self.__quality) + "\n"
        string += "Age: " + str(self.age) + "\n"
        for n, cell in enumerate(self.__cells, start=1):
            string += "Cell {}:\n".format(n) + str(cell) + "\n"
        return string

    def duplicate(self):
        """
        Duplicates the EpigeneticIndividual.

        :return: a copy of the EpigeneticIndividual.
        :rtype: EpigeneticIndividual
        """
        temp_list = list()
        for c in self.__cells:
            temp_list.append(EpigeneticCell(
                self.__problem, c.nucleosomes, c.solution, c.fitness))
        fem = EpigeneticIndividualFemale(
            self.__problem, len(temp_list), temp_list)
        fem.age = self.age
        fem.quality = self.quality
        return fem

    def replicate(self):
        """
        Duplicates the EpigeneticIndividual and set all its cells as a copy of the best EpigeneticCell.

        :return: a copy of the EpigeneticIndividual.
        :rtype: EpigeneticIndividual
        """
        temp_list = list()
        c = self.get_best_cell()
        for i in range(len(self.__cells)):
            temp_list.append(c.duplicate())
        return EpigeneticIndividualFemale(self.__problem, len(temp_list), temp_list)

    def evaluate(self):
        """Evaluates the EpigeneticIndividual by evaluating of the EpigeneticCells in it."""
        for cell in self.__cells:
            cell.evaluate()

    def get_best_cell(self):
        """
        Returns the best EpigeneticCell in the EpigeneticIndividual.

        :return: the best EpigeneticCell in the EpigeneticIndividual.
        :rtype: EpigeneticCell
        """
        best_c = None
        for c in self.__cells:
            if c.is_better(best_c):
                best_c = c
        return best_c

    def get_worst_cell(self):
        """
        Returns the worst EpigeneticCell in the EpigeneticIndividual.

        :return: the best EpigeneticCell in the EpigeneticIndividual.
        :rtype: EpigeneticCell
        """
        worst_c = None
        for i in range(len(self.cells)):
            if i == 0:
                if self.cells[i].is_better(self.cells[i+1]):
                    worst_c = self.cells[i+1]
                else:
                    worst_c = self.cells[i]
            else:
                if worst_c.is_better(self.cells[i]):
                    worst_c = self.cells[i]

        return worst_c

    def get_best_fitness(self):
        """
        Returns the fitness of the best EpigeneticCell in the EpigeneticIndividual.

        :return: the fitness of the best EpigeneticCell in the EpigeneticIndividual.
        :rtype: float
        """
        return self.get_best_cell().fitness

    def is_better(self, individual):
        """
        Returns **True** if the EpigeneticIndividual is better than ``individual``.

        :param EpigeneticIndividual individual: the individual.
        :return: True if the EpigeneticIndividual is better than individual.
        :rtype: bool
        """

        return self.__quality >= individual.__quality and self.get_best_cell().is_better(individual.get_best_cell())
        # return self.get_best_cell().is_better(individual.get_best_cell())

    def eval_individual_fitness(self, male):
        # w1 (competitividad)
        w1 = 1.
        # w2 (edad y fertilidad)
        w2 = 0.
        # w3 (cooperacion)
        w3 = 0.
        fx = self.get_best_cell().fitness
        # b = 0  # female benefits
        q = male.quality
        fy = male.get_best_cell().fitness
        mu = 5
        sigma = 2
        g = 1 - (abs(self.age - mu)) / sigma
        self.quality = (w1 * fy + w2 * fx + w3 * g) / (w1 + w2 + w3)

    def replace_worst(self, new_cell):
        """
        Replaces worst cell with a new one created externally in reproduction

        :param EpigeneticCell new_cell: the cell for replacing
        """

        worst_c = self.get_worst_cell()
        for cell in self.__cells:
            if cell == worst_c:
                self.cells.remove(cell)
                self.cells.append(new_cell)

    def add_cell(self, cell):
        self.__cells.append(cell)

    def calc_investment(self, male):
        pass

    @property
    def cells(self):
        """Returns the EpigeneticCells in the EpigeneticIndividual.

        :return: the EpigeneticCells in the EpigeneticIndividual.
        :rtype: list[EpigeneticCell]
        """
        return self.__cells

    @property
    def id(self):
        return self.__id

    @property
    def energy(self):
        return self.__energy

    @energy.setter
    def energy(self, value):
        self.__energy = value

    @property
    def age(self):
        return self.__age

    @age.setter
    def age(self, value):
        self.__age = value

    @property
    def quality(self):
        return self.__quality

    @quality.setter
    def quality(self, value):
        self.__quality = value

    @property
    def problem(self):
        """
        Returns the Problem.

        :return: the problem.
        :rtype: Problem
        """
        return self.__problem
