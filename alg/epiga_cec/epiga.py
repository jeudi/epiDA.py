# -*- coding: utf-8 -*-
# file  :  epiga.py
# author:  Daniel H. Stolfi
# date  :  2019-08-15
#
# Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics.
# In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
# doi> 10.1016/j.ins.2017.10.005


from .epigeneticpopulationmale import EpigeneticPopulationMale
from .epigeneticpopulationfemale import EpigeneticPopulationFemale
from .operators.nucleosomegenerator import NucleosomeGenerator
from .operators.selectionoperator import SelectionOperator
from .operators.reproductionoperator import ReproductionOperator
from .operators.epigeneticoperator import EpigeneticOperator
from .operators.replacementoperator import ReplacementOperator
from .problems.problem import Problem


class EpiGA:
    """epiGenetic Algorithm."""

    def __init__(self, problem, n_individuals, n_cells, nucleosomes, selection, reproduction, epigenetic, replacement):
        """
        Constructor.

        :param Problem problem: the problem.
        :param int n_individuals: the number of individuals.
        :param int n_cells: the number of cells.
        :param NucleosomeGenerator nucleosomes: the nucleosome generator.
        :param SelectionOperator selection: the selection operator.
        :param ReproductionOperator reproduction: the reproduction operator.
        :param EpigeneticOperator epigenetic: the epigenetic operator.
        :param ReplacementOperator replacement: the replacement operator.
        """
        self.__problem = problem
        self.__n_individuals = n_individuals
        self.__n_cells = n_cells

        self.__nucleosomes = nucleosomes
        self.__selection = selection
        self.__reproduction = reproduction
        self.__epigenetic = epigenetic
        self.__replacement = replacement

    def run(self, n_evaluations, verbosity=0, fem_solution_file=None, male_solution_file=None, fem_stat_file=None, male_stat_file=None):
        """
        Begins the execution and returns the best fitness.

        :param int n_evaluations: the maximum number of evaluations.
        :param int verbosity: the verbosity level [0 -- 4].
        :param str solution_file: the solution file.
        :param str stat_file: the stats file (convergence data).
        :return: the best fitness.
        :rtype: float
        """
        gen = 1
        f_stat = None
        m_stat = None

        if fem_stat_file:
            f_stat = open(fem_stat_file, "w")

        if male_stat_file:
            m_stat = open(male_stat_file, "w")

        male_pop = EpigeneticPopulationMale(self.__problem)
        male_pop.generate(self.__n_individuals, self.__n_cells, gen)
        fem_pop = EpigeneticPopulationFemale(self.__problem)
        fem_pop.generate(self.__n_individuals, self.__n_cells, gen)

        # if verbosity > 3:
        #     self.__debug("Initialization", male_pop)
        #     self.__debug("Initialization", fem_pop)

        ev = self.__problem.evaluations
        while ev <= n_evaluations:

            if verbosity > 3:
                self.__debug("New generation", male_pop)
                self.__debug("New generation", fem_pop)

            # Let's select!
            # only males are selected

            tempmale = self.__selection.select(male_pop, "m")
            self.__nucleosomes.generate(tempmale)

            tempfem = self.__selection.select(fem_pop, "f", tempmale)
            self.__nucleosomes.generate(tempfem)

            if verbosity > 3:
                self.__debug("Selection and Nucleosomes", tempmale)
                self.__debug("Selection and Nucleosomes", tempfem)

            # let's do the crossover

            daughters, sons = self.__reproduction.reproduction(
                tempmale, tempfem, self.__problem, self.__n_cells, gen)
            if verbosity > 3:
                self.__debug("Reproduction", daughters)
                self.__debug("Reproduction", sons)

            # let's methylate

            self.__epigenetic.methylate(sons)
            sons.evaluate()
            self.__epigenetic.methylate(daughters)
            daughters.evaluate()

            if verbosity > 3:
                self.__debug("Methylation", daughters)
                self.__debug("Methylation", sons)

            # let's replace

            male_pop = self.__replacement.replace(male_pop, sons, "m")
            # male_pop = sons
            # fem_pop = daughters
            fem_pop = self.__replacement.replace(fem_pop, daughters, "f")

            # update age
            for i in range(fem_pop.size):
                fem_pop.get(i).age += 1

            ev = self.__problem.evaluations

            if verbosity > 3:
                self.__debug("Evaluation and Replacement", fem_pop)
                self.__debug("Evaluation and Replacement", male_pop)

            if verbosity > 0 or f_stat:
                avgm, bestm = male_pop.metrics()
                avgf, bestf = fem_pop.metrics()
                if verbosity > 0:
                    print("Males:")
                    print("Generations: {:6d}  Evaluations: {:6d}  Average Fitness: {:12.10f}  Best Fitness: {:12.10f}".
                          format(gen, ev, avgm, bestm))
                    print("Females:")
                    print("Generations: {:6d}  Evaluations: {:6d}  Average Fitness: {:12.10f}  Best Fitness: {:12.10f}".
                          format(gen, ev, avgf, bestf))
                if f_stat:
                    f_stat.write("{}\t{}\t{}\t{}\n".format(
                        gen, ev, avgf, bestf))
                if m_stat:
                    m_stat.write("{}\t{}\t{}\t{}\n".format(
                        gen, ev, avgm, bestm))

            gen += 1

        best_cell_fem = fem_pop.best_individual().get_best_cell()
        best_cell_male = male_pop.best_individual().get_best_cell()
        if verbosity > 2:
            print("Best cell fem: ", best_cell_fem)
            print("Best cell male: ", best_cell_male)
        if f_stat:
            f_stat.close()
        if fem_solution_file:
            f_sol = open(fem_solution_file, "w")
            for i in best_cell_fem.solution:
                f_sol.write(str(i))
                f_sol.write(" ")
            f_sol.write("\n")
            f_sol.close()
        if male_solution_file:
            f_sol = open(male_solution_file, "w")
            for i in best_cell_male.solution:
                f_sol.write(str(i))
                f_sol.write(" ")
            f_sol.write("\n")
            f_sol.close()

        return fem_pop.best_individual().get_best_cell().fitness, male_pop.best_individual().get_best_cell().fitness

    @staticmethod
    def __debug(title, pop):
        print(title)
        print(pop)
