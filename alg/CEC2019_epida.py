#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# file  :  EggholderFunction.py
# author:  Daniel H. Stolfi
# date  :  2019-08-15
#
# Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics.
# In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
# doi> 10.1016/j.ins.2017.10.005
# -*- coding: utf-8 -*-
"""
Example of the epiGenetic Algorithm solving the Eggholder Function problem (Minimization, Integer Representation).
"""
import math
import numpy as np
import random as rand
import sys
from datetime import date
import os

from epiga_cec.problems.floatproblem import FloatProblem
from epiga_cec.epiga import EpiGA
from epiga_cec.operators.nucleosomegenerator import NucleosomeGenerator
from epiga_cec.operators.dahselection import DAHSelection
from epiga_cec.operators.nucleosomebasedreproduction import NucleosomeBasedReproduction
from epiga_cec.operators.numericgenesilencing import NumericGeneSilencing
from epiga_cec.operators.elitistreplacement import ElitistReplacement
from epiga_cec.operators.dahreplacement import DAHReplacement
from cec2019comp100digit import cec2019comp100digit


class CEC2019_epida(FloatProblem):
    """CEC Function Problem."""

    def __init__(self, prob, min, max, dim):
        super().__init__("CEC Function " + str(prob), dim,
                         [min for i in range(dim)], [max for i in range(dim)], [0.5 for i in range(dim)], FloatProblem.MINIMIZATION)
        self.bench = cec2019comp100digit
        self.bench.init(prob, dim)

    def _get_fitness(self, solution):
        sol = np.array(solution)
        ev = self.bench.eval(sol)
        return ev

    def repair(self, solution):
        return solution


if __name__ == '__main__':
    # problem selection
    ind = int(sys.argv[1])
    cells = int(sys.argv[2])
    nuc_prob = float(sys.argv[3])
    silence_prob = float(sys.argv[4])
    rad = int(sys.argv[5])
    prob = int(sys.argv[6])
    it = int(sys.argv[7])

    if prob == 1:
        min = -8192
        max = 8192
        dim = 9
    elif prob == 2:
        min = -16384
        max = 16384
        dim = 16
    elif prob == 3:
        min = -4
        max = 4
        dim = 18
    elif 4 <= prob <= 10:
        min = -100
        max = 100
        dim = 10
    else:
        print("no go")
        sys.exit()

    date = str(date.today())
    directory = "data" + "/" + date
    PARENT_FOLDER = os.path.dirname(os.path.abspath(__file__))
    path = os.path.join(PARENT_FOLDER, directory)

    try:
        os.makedirs(path)
        # print("Directory '%s' created successfully" % directory)
    except OSError as error:
        # print("Directory '%s' can not be created, already exists" % directory)
        pass

    for i in range(10):
        f_name = "report_stats_fem_tuning_" + date + \
            "_" + str(prob) + "_" + str(i) + ".txt"
        fem_stats_file = os.path.join(path, f_name)

        f_name = "report_stats_male_tuning_" + date + \
            "_" + str(prob) + "_" + str(i) + ".txt"
        male_stats_file = os.path.join(path, f_name)

        f_name = "report_best_fem_tuning_" + date + \
            "_" + str(prob) + "_" + str(i) + ".txt"
        fem_best_file = os.path.join(path, f_name)

        f_name = "report_best_male_tuning_" + date + \
            "_" + str(prob) + "_" + str(i) + ".txt"
        male_best_file = os.path.join(path, f_name)

        p = CEC2019_epida(prob, min, max, dim)

        alg = EpiGA(
            p,
            ind,
            cells,
            NucleosomeGenerator(nuc_prob, rad),
            DAHSelection(),
            NucleosomeBasedReproduction(),
            NumericGeneSilencing(silence_prob),
            DAHReplacement(duplicates=True)
        )

        alg.run(it, 4, fem_best_file, male_best_file,
                fem_stats_file, male_stats_file
                )
