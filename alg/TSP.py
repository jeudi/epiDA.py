#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# file  :  OneMax.py
# author:  Daniel H. Stolfi
# date  :  2019-08-15
#
# Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics.
# In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
# doi> 10.1016/j.ins.2017.10.005
# -*- coding: utf-8 -*-
"""
Example of the epiGenetic Algorithm solving the Traveling Salesman Problem (Minimization, Permutation Representation).
"""
from epiga.operators.permutationgenesilencing import PermutationGeneSilencing
from epiga.problems.permutationproblem import PermutationProblem
from epiga.epiga import EpiGA
from epiga.operators.nucleosomegenerator import NucleosomeGenerator
from epiga.operators.binarytournamentselection import BinaryTournamentSelection
from epiga.operators.nucleosomebasedreproduction import NucleosomeBasedReproduction
from epiga.operators.elitistreplacement import ElitistReplacement


class TSP(PermutationProblem):
    """Travelling Salesman Problem."""

    def __init__(self, distances):
        """
        Constructor.

        :param list[list[int]] distances: the distances between cities.
        """
        self.__distances = distances
        cities = len(distances[0])
        super().__init__("TSP", list(range(cities)), [0.5] * cities, PermutationProblem.MINIMIZATION)

    def __str__(self):
        str_tsp = super().__str__() + "\nDistances:\n"
        for row in range(len(self.__distances)):
            for col in range(len(self.__distances[row])):
                str_tsp += "{:3d}".format(self.__distances[row][col])
            str_tsp += "\n"
        return str_tsp

    def _get_fitness(self, solution):
        prev = 0
        fitness = 0
        for city in solution[1:]:
            fitness += self.__distances[prev][city]
            prev = city
        fitness += self.__distances[prev][0]
        return fitness

    def repair(self, solution):
        new_sol = super().repair(solution)
        if new_sol[0] != 0:
            city = new_sol[0]
            new_sol[new_sol.index(0)] = city
            new_sol[0] = 0
        return new_sol


if __name__ == '__main__':
    M = [[0, 16, 30, 12, 4, 13, 2, 30, 12, 6, 21, 2, 19, 7, 19, 16, 25, 9, 9, 3],
         [16, 0, 30, 30, 29, 22, 19, 8, 7, 24, 18, 8, 28, 16, 7, 2, 12, 30, 26, 1],
         [30, 30, 0, 10, 22, 26, 7, 30, 15, 13, 16, 1, 17, 2, 17, 4, 11, 21, 24, 26],
         [12, 30, 10, 0, 26, 14, 6, 2, 25, 9, 17, 13, 27, 29, 8, 22, 22, 24, 19, 29],
         [4, 29, 22, 26, 0, 21, 13, 13, 12, 14, 2, 10, 6, 5, 26, 14, 24, 27, 27, 21],
         [13, 22, 26, 14, 21, 0, 27, 20, 30, 23, 25, 16, 12, 7, 25, 23, 18, 10, 8, 1],
         [2, 19, 7, 6, 13, 27, 0, 16, 24, 21, 29, 18, 25, 22, 17, 5, 12, 21, 8, 1],
         [30, 8, 30, 2, 13, 20, 16, 0, 8, 30, 19, 19, 8, 3, 19, 5, 15, 23, 2, 20],
         [12, 7, 15, 25, 12, 30, 24, 8, 0, 7, 27, 5, 30, 10, 12, 21, 16, 12, 18, 11],
         [6, 24, 13, 9, 14, 23, 21, 30, 7, 0, 14, 5, 11, 27, 23, 25, 6, 13, 4, 19],
         [21, 18, 16, 17, 2, 25, 29, 19, 27, 14, 0, 22, 18, 17, 3, 29, 24, 13, 19, 17],
         [2, 8, 1, 13, 10, 16, 18, 19, 5, 5, 22, 0, 7, 14, 16, 21, 22, 8, 27, 27],
         [19, 28, 17, 27, 6, 12, 25, 8, 30, 11, 18, 7, 0, 9, 7, 29, 5, 30, 27, 14],
         [7, 16, 2, 29, 5, 7, 22, 3, 10, 27, 17, 14, 9, 0, 19, 21, 25, 26, 5, 16],
         [19, 7, 17, 8, 26, 25, 17, 19, 12, 23, 3, 16, 7, 19, 0, 6, 26, 2, 7, 4],
         [16, 2, 4, 22, 14, 23, 5, 5, 21, 25, 29, 21, 29, 21, 6, 0, 16, 10, 25, 22],
         [25, 12, 11, 22, 24, 18, 12, 15, 16, 6, 24, 22, 5, 25, 26, 16, 0, 22, 26, 16],
         [9, 30, 21, 24, 27, 10, 21, 23, 12, 13, 13, 8, 30, 26, 2, 10, 22, 0, 18, 21],
         [9, 26, 24, 19, 27, 8, 8, 2, 18, 4, 19, 27, 27, 5, 7, 25, 26, 18, 0, 22],
         [3, 1, 26, 29, 21, 1, 1, 20, 11, 19, 17, 27, 14, 16, 4, 22, 16, 21, 22, 0]]
    p = TSP(M)
    alg = EpiGA(p, 40, 1, NucleosomeGenerator(3 / len(M[0]), 4), BinaryTournamentSelection(),
                NucleosomeBasedReproduction(), PermutationGeneSilencing(0.05),
                ElitistReplacement(duplicates=False))
    alg.run(50000, 3)  # 77
